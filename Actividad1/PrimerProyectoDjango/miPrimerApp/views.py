from django.shortcuts import render, HttpResponse
from .models import *
from django.db.models import Count

# Create your views here.
def index(request):

    estudiantesGrupo1 = Estudiante.objects.filter(grupo=1)

    estudiantesGrupo4 = Estudiante.objects.filter(grupo=4)

    mismosApellidos = Estudiante.objects.filter(apellidos__in=Estudiante.objects.values('apellidos').annotate(total=Count('apellidos')).filter(total__gt=1).values('apellidos')).order_by('apellidos')

    mismaEdad = Estudiante.objects.filter(edad__in=Estudiante.objects.values('edad').annotate(total=Count('edad')).filter(total__gt=1).values_list('edad', flat=True)).order_by('edad')

    grupo3_mismaEdad = Estudiante.objects.filter(grupo=3, edad__in=Estudiante.objects.filter(grupo=3).values('edad').annotate(count_edad=Count('edad')).filter(count_edad__gt=1).values('edad'))

    estudiantes = Estudiante.objects.all()

    # Agregamos la lista al contexto
    context = {'estudiantesGrupo1': estudiantesGrupo1, 
               'estudiantesGrupo4': estudiantesGrupo4, 
               'mismosApellidos': mismosApellidos, 
               'mismaEdad': mismaEdad,
               'grupo3_mismaEdad': grupo3_mismaEdad,
               'estudiantes': estudiantes
               }
    
    return render(request, 'index.html', context)